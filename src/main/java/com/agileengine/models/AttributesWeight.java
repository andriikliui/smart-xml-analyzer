package com.agileengine.models;

import java.util.Arrays;

public enum AttributesWeight {

    ID("id", 3),
    CLASS("class", 2),
    HREF("href", 1),
    TITLE("title", 1),
    REL("rel", 1),
    ON_CLICE("onclick", 1),
    TAG("tag", 2),
    CONTENT("content", 2);

    public String name;
    public Integer weight;

    AttributesWeight(String name, Integer weight) {
        this.name = name;
        this.weight = weight;
    }

    public static Integer getWeightByAttributeName(String name){

        return Arrays.asList(AttributesWeight.values())
                .stream()
                .filter(attribute -> name.equals(attribute.name))
                .map(attribute -> attribute.weight)
                .findFirst().orElse(Integer.valueOf(0));
    }

}
