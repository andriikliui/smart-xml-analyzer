package com.agileengine;

import org.jsoup.nodes.Element;

import java.io.File;
import java.util.Set;

import static com.agileengine.JsoupSearchUtil.buildPath;

public class MainApp {


    public static void main(String[] args) {

        if(args.length != 2){
            throw new IndexOutOfBoundsException("There are two required fields  <input_origin_file_path> <input_other_sample_file_path>");
        }

        String TARGET_ELEMENT_ID = "make-everything-ok-button";
        String originFilePath = args[0];
        String newSampleFilePath = args[1];

        Element originElement = JsoupSearchUtil.findElementById(new File(originFilePath), TARGET_ELEMENT_ID).get();

        Set<Element> similarElements = JsoupSearchUtil.findSimilarElements(new File(newSampleFilePath), originElement);
        Element similarElement = JsoupSearchUtil.findTheMostSimilar(originElement, similarElements);


        System.out.println("---------");
        System.out.println("Origin element: " + originElement);
        System.out.println(buildPath(originElement));

        System.out.println("---------");

        System.out.println("Similar Element: "  + similarElement);
        System.out.println(buildPath(similarElement));
        System.out.println("---------");
    }

}
