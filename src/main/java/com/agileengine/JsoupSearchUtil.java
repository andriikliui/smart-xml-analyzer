package com.agileengine;

import com.agileengine.models.AttributesWeight;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public class JsoupSearchUtil {

    private static Logger LOGGER = LoggerFactory.getLogger(JsoupSearchUtil.class);

    private static String CHARSET_NAME = "utf8";

    public static Optional<Element> findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return Optional.of(doc.getElementById(targetElementId));

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    public static Set<Element> findSimilarElements(File htmlFile, Element targetElement) {
        try {
            Set<Element> elements = new HashSet<>();
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            targetElement.attributes().asList()
                    .stream()
                    .forEach(attribute ->
                           doc.getElementsByAttributeValue(attribute.getKey(), attribute.getValue())
                                   .stream()
                                   .forEach(element ->elements.add(element)));
            return elements;
        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return new HashSet();
        }
    }


    public static Element findTheMostSimilar(Element standard, Set<Element> targetElements) {

        Map<Integer, Element> elementsWithWeight = new HashMap<>();
        Map<String, String> standardAttributesDataSet = standard.attributes()
                .asList()
                .stream()
                .collect(Collectors.toMap(Attribute::getKey, Attribute::getValue));

        for (Element element: targetElements) {
            Integer weight = element.attributes().asList()
                    .stream()
                    .filter(attribute -> standardAttributesDataSet.containsKey(attribute.getKey()))
                    .filter(attribute -> attribute.getValue().equals(standardAttributesDataSet.get(attribute.getKey())))
                    .mapToInt(attribute -> AttributesWeight.getWeightByAttributeName(attribute.getKey()))
                    .sum();

            if(weight != 0) {
                elementsWithWeight.put(weight, element);
            }
        }

        Integer maxWeight = elementsWithWeight.keySet().stream().max(Integer::compareTo).orElse(0);

        return elementsWithWeight.get(maxWeight);
    }


    public static String buildPath(Element element){
        StringBuilder path = new StringBuilder();

        element.parents()
                .stream()
                .map(ele -> ele.tagName())
                .collect(Collectors.toCollection(LinkedList::new))
                .descendingIterator()
                .forEachRemaining(tag -> path.append(tag + " > "));

        String tags = path.toString() + element.tagName();

        return tags;
    }


}