# AgileEngine backend-XML java snippets

It is built on top of [Jsoup](https://jsoup.org/).

You can use Jsoup for your solution or apply any other convenient library. 

# Execution

java -jar smart-XML-analyzer-0.0.1.jar ./samples/sample-0-origin.html ./samples/sample-1-evil-gemini.html

java -jar smart-XML-analyzer-0.0.1.jar ./samples/sample-0-origin.html ./samples/sample-2-container-and-clone.html

java -jar smart-XML-analyzer-0.0.1.jar ./samples/sample-0-origin.html ./samples/sample-3-the-escape.html

java -jar smart-XML-analyzer-0.0.1.jar ./samples/sample-0-origin.html ./samples/sample-4-the-mash.html


# Comparison output for sample pages. 

1) 
---------
Origin element: <a id="make-everything-ok-button" class="btn btn-success" href="#ok" title="Make-Button" rel="next" onclick="javascript:window.okDone(); return false;"> Make everything OK </a>
html > body > div > div > div > div > div > div > a
---------
Similar Element: <a class="btn btn-success" href="#check-and-ok" title="Make-Button" rel="done" onclick="javascript:window.okDone(); return false;"> Make everything OK </a>
html > body > div > div > div > div > div > div > a
---------

2)
---------
Origin element: <a id="make-everything-ok-button" class="btn btn-success" href="#ok" title="Make-Button" rel="next" onclick="javascript:window.okDone(); return false;"> Make everything OK </a>
html > body > div > div > div > div > div > div > a
---------
Similar Element: <a class="btn test-link-ok" href="#ok" title="Make-Button" rel="next" onclick="javascript:window.okComplete(); return false;"> Make everything OK </a>
html > body > div > div > div > div > div > div > div > a
---------

3) 
---------
Origin element: <a id="make-everything-ok-button" class="btn btn-success" href="#ok" title="Make-Button" rel="next" onclick="javascript:window.okDone(); return false;"> Make everything OK </a>
html > body > div > div > div > div > div > div > a
---------
Similar Element: <a class="btn btn-success" href="#ok" title="Do-Link" rel="next" onclick="javascript:window.okDone(); return false;"> Do anything perfect </a>
html > body > div > div > div > div > div > div > a
---------

4) 
---------
Origin element: <a id="make-everything-ok-button" class="btn btn-success" href="#ok" title="Make-Button" rel="next" onclick="javascript:window.okDone(); return false;"> Make everything OK </a>
html > body > div > div > div > div > div > div > a
---------
Similar Element: <a class="btn btn-success" href="#ok" title="Make-Button" rel="next" onclick="javascript:window.okFinalize(); return false;"> Do all GREAT </a>
html > body > div > div > div > div > div > div > a
---------


